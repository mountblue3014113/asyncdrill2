const fs = require("fs");

function getListsOfCards(listID, callback) {
  setTimeout(() => {
    fs.readFile("../data/cards.json", "utf8", (err, data) => {
      if (err) {
        callback(err, null);
        return;
      } else {
        const cardLists = JSON.parse(data);
        const lists = cardLists[listID];
        if (lists) {
          callback(null, lists);
        } else {
          callback(err, null);
        }
      }
    });
  }, 3 * 1000);
}

module.exports = getListsOfCards;
