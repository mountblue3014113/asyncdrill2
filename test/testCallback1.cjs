const getBoardInfo = require("../callback1.cjs");

getBoardInfo("abc122dc", (err, board) => {
  if (err) {
    console.error(err.message);
  } else {
    console.log(board);
  }
});
