const getBoardInfo = require("./callback1.cjs");
const getListsForBoard = require("./callback2.cjs");
const getListsOfCards = require("./callback3.cjs");

function allCardsForAllLists(boardID) {
  setTimeout(() => {
    getBoardInfo(boardID, (err, board) => {
      if (err) {
        console.error(err);
      } else {
        console.log("Thanos Board info:", board);
      }

      getListsForBoard(board.id, (err, lists) => {
        if (err) {
          console.error(err);
        } else {
          // console.log(lists);
          for (let key in lists) {
            setTimeout(() => {
              console.log("\nThanos Board list:", lists[key]);
              getListsOfCards(lists[key].id, (err, cards) => {
                if (err) {
                  console.error(err);
                } else {
                  if(cards){
                    console.log(`\nCards for ${lists[key].name} list:`, cards);
                  }
                  else{
                    console.log(`\nNo cards for ${lists[key].name} list`);
                  }
                }
              });
            }, key * 5000);
          }
        }
      });
    });
  }, 1000);
}

module.exports = allCardsForAllLists;
