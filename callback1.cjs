const fs = require('fs');

function getBoardInfo(boardID, callback) {
  setTimeout(() => {
    fs.readFile('../data/boards.json', 'utf8', (err, data) => {
      if (err) {
        callback(err, null);
        return;
      }else{
        const boards = JSON.parse(data);
        const board = boards.find(board => board.id === boardID);
        if (board) {
          callback(null,board);
        } else {  
          callback(new Error('Board not found'), null);
        }
      }
    });
  }, 3*1000); 
}

module.exports = getBoardInfo;
