const fs = require("fs");

function getListsForBoard(boardID, callback) {
  setTimeout(() => {
    fs.readFile("../data/lists_1.json", "utf8", (err, data) => {
      if (err) {
        callback(err, null);
        return;
      } else {
        const listsData = JSON.parse(data);
        const lists = listsData[boardID];
        if (lists) {
          callback(null, lists);
        } else {
          callback(err, null);
        }
      }
    });
  }, 3 * 1000);
}

module.exports = getListsForBoard;
