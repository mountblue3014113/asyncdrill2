const getBoardInfo = require("./callback1.cjs");
const getListsForBoard = require("./callback2.cjs");
const getListsOfCards = require("./callback3.cjs");

function cardsForMindList(boardId) {
  setTimeout(() => {
    getBoardInfo(boardId, (err, board) => {
      if (err) {
        console.error(err);
      } else {
        console.log("Information for Thanos Board:", board);
      }
      getListsForBoard(board.id, (err, lists) => {
        if (err) {
          console.error(err);
        } else {
          setTimeout(() => {
            for (let key in lists) {
              if (lists[key].name === "Mind") {
                console.log("Lists for Thanos Board:",lists[key]);
                getListsOfCards(lists[key].id, (err, cards) => {
                  console.log("cards for the Mind list",cards);
                });
              } else {
                setTimeout(() => {
                  console.log(lists[key]);
                }, 5000);
              }
            }
            //   console.log(lists);
          }, 5000);
        }
      });
    });
  }, 1 * 1000);
}

module.exports = cardsForMindList;
